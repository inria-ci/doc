# CI documentation

This project builds the https://inria-ci.gitlabpages.inria.fr/doc website (also redirected to https://ci.inria.fr/doc)
using [hugo](https://gohugo.io/) and [Ananke theme](https://github.com/theNewDynamic/gohugo-theme-ananke).

Ananke is a git submodule of this git project, and it is up to the developper to fetch it for the website to be
buildable.

## Serve the documentation locally with hugo

Hugo must be pre-installed on your machine, then:

```bash
hugo --config localconfig.toml server
```

```txt
Web Server is available at http://localhost:1313/doc/ (bind address 0.0.0.0)
Press Ctrl+C to stop
```

## Serve the documentation inside a docker container

```bash
docker pull klakegg/hugo:0.107.0 # Not mandatory if already pulled
docker run --rm -it -v $(pwd):/src -p 1313:1313 klakegg/hugo:0.107.0 server
```

```txt
Web Server is available at http://localhost:1313/doc/ (bind address 0.0.0.0)
Press Ctrl+C to stop
```

## History of this project

Contents has been imported from https://wiki.inria.fr/ciportal/ with
the script import_mediawiki/import_mediawiki.py
