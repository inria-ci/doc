+++
title =  "ci-ssh upgrade, 13 April 2021 9:30am-10:00am"
date = "2021-04-12T14:52:50+02:00"
tags = []
+++

The `ci-ssh` machine will be upgraded on 13 April 2021 between 9:30am and 10:00am,
and will be unavailable for about 5 minutes during this interval.

This machine is the proxy used for connecting CI VMs by ssh, therefore the VMs
will be unavailable as well during this short period of time.

This upgrade will provide a more recent version of `OpenSSH` (`8.0p`
instead of `5.3p1`), which will bring support for ed25519 keys and
other new cyphering algorithms.

Thank you for your understanding!
