+++
title =  "Planned shutdown for Apple hypervisors: June 26 6pm till June 27 9pm"
date = "2023-06-23T19:22:52+01:00"
tags = ["featured"]
+++

Apple hypervisors will be shutdown from Monday 26 June 6pm till
Tuesday 27 June 9pm for maintainance.

Sorry for the inconvenience.
