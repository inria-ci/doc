+++
title =  "CloudStack is now updated to 4.16"
date = "2023-02-08T14:32:07+01:00"
tags = ["featured"]
+++

CloudStack is now updated to 4.16. You should now be able to start
your virtual machines again via the portal ci.inria.fr or via
CloudStack API or web interface.

You may refer to the portal documentation: https://inria-ci.gitlabpages.inria.fr/doc/page/web_portal_tutorial/#slaves

Thanks to this update, Mac OS Catalina and Mojave are supported, and
nested KVM is enabled.

Thank you very much for using CI.
