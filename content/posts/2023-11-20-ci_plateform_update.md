+++
title =  "CI plateform interruption"
date = "2023-11-20T15:18:30+0100"
tags = ["featured"]
+++

We will be updating and restarting some machines in charge of our
infrastructure tomorrow, Tuesday, November 21, between 1:30 pm and 2:00
pm. This update is expected to be brief. While the virtual machines
should not be impacted, all services related to user accounts,
including
login, will be temporarily unavailable.

We apologize for any inconvenience and will endeavor to minimize the
downtime. Thank you for your understanding. 