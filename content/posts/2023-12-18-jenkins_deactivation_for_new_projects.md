+++
title =  "Jenkins deactivation for new projects"
date = "2023-12-18T09:44:00+0100"
tags = ["featured"]
+++

Chers utilisateurs CI,

Rencontrant pas mal de problèmes de maintenance sur les serveurs Jenkins, nous avons décidé d'enlever de l'interface utilisateur la possibilité d'activer Jenkins à la création d'un projet CI. Cependant, vous pourrez toujours créer un ticket helpdesk (https://helpdesk.inria.fr/categories/320/submit) si vous souhaitez avoir Jenkins.

Cordialement,

L'équipe CI

--------------------
Dear CI users,

As we are regularly facing maintenance issues with Jenkins servers, we have decided to remove from the user interface the option to activate Jenkins when creating a CI project. However, you will still be able to create a helpdesk ticket (https://helpdesk.inria.fr/categories/320/submit) if you wish to use Jenkins.

Best regards,

The CI Team

