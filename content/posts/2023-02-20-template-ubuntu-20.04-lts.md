+++
title =  "Featured template update for Ubuntu 20.04 LTS amd64"
date = "2023-02-20T19:22:52+01:00"
tags = ["featured"]
+++

There is a new featured template for Ubuntu 20.04 LTS amd64
(`ubuntu-20.04-lts`). This template subsumes the former template
`Ubuntu 20.04 amd64`, which is no longer featured.

Changes with respect to the former template:
- the initialization service for '~ci/.ssh/authorized_keys` is fixed,
  and the contents of this file will no longer be reset on each boot;
- cloud-init is now configured to consider CloudStack provided
  user-data by default;
- packages are upgraded to their last available version.

Thank you very much for using CI.
