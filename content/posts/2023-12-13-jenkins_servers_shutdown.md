+++
title =  "Reboot of the Jenkins servers"
date = "2023-12-13T13:30:00+0100"
tags = ["featured"]
+++

A maintenance operation requires us to stop all CI jenkins servers. This operation will take place tomorrow, 
Thursday December 14 at 1:30 pm. It should last half an hour.

Once the servers have been restarted, your jenkins projects may remain switched off.
You may have to restart them manually on ci.inria.fr.

We apologize for the inconvenience. 