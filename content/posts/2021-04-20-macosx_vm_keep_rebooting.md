+++
title =  "Mac OS X VMs keep rebooting"
date = "2021-04-20T15:47:40+02:00"
tags = ["featured"]
+++

For a few days and for an unknown reason, Mac OS X VMs are
continuously rebooting when they are newly created or when their offer
is changed. We are aware of the problem and are trying to fix it
as soon as possible.

Thank you for your understanding!
