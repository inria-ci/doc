+++
title =  "LDAP certificate renewal"
date = "2022-04-04T10:20:17+02:00"
tags = ["featured"]
+++

The certificate of the LDAP server was expired this morning and has
been renewed. You may have had some difficulties to connect to
ci.inria.fr, CloudStack and Jenkins instances this morning but
everything should work again now! Sorry for the inconvenience.
