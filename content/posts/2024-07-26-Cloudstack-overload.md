+++
title =  "Cloudsatck Overload"
date = "2024-07-26T10:13:00+0100"
tags = ["featured"]
+++

Chers utilisateurs CI,

Depuis quelques temps nous constatons des difficultés pour démarer des VMs Cloudstack utilisant un grand nombre de coeurs CPU. Le démarage est en erreur avec un message "Unable to start a VM due to insufficient capacity". Nous cherchons un moyen d'accroitre les capacités CPU du service mais celà va prendre du temps.

En attendant, pour que chacun puisse profiter pleinement de Cloudstack **nous vous enjoignons fortement à éteindre toutes les VMs qui ne servent pas**. Vous pouvez égelement utiliser un quadriciel comme opentofu pour créer et supprimer des VMs à la demande. Vous trouverez un exemple d'utilisation d'opentofu dans la gallery d'exemples CI [ici](https://gitlab.inria.fr/gitlabci_gallery/orchestration/opentofu).


Cordialement,

L'équipe CI

--------------------

Dear CI users,

For some time now we've been experiencing difficulties starting Cloudstack VMs using a large number of CPU cores. The startup error is “Unable to start a VM due to insufficient capacity”. We're looking for a way to increase the service's CPU capacity, but it's going to take some time.

In the meantime, so that everyone can take full advantage of Cloudstack **we strongly urge you to turn off all VMs that are not in use**. You can also use a framework like opentofu to create and delete VMs on demand. You'll find an example of how to use opentofu in the CI example gallery [here](https://gitlab.inria.fr/gitlabci_gallery/orchestration/opentofu).


Best,

The CI Team


