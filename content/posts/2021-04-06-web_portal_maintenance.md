+++
title =  "Web portal maintenance, 6 April 2021 6pm-6:30pm"
date = "2021-04-06T16:26:11+02:00"
tags = []
+++

The web portal ci.inria.fr may be unavailable on 6 April 2021
from 6pm till 6:30pm for maintenance purposes (LDAP).
It concerns only the web portal. CloudStack, virtual machines, Jenkins
and gitlab-ci shall not be impacted.

Thank you for your understanding!
