+++
title =  "Deletion of old Jenkins servers"
date = "2024-07-02T10:37:00+0100"
tags = ["featured"]
+++

Chers utilisateurs CI,

Suite aux rappels envoyés par e-mail début mai et début juin, nous vous informons que les serveurs Jenkins dont la 
version est antérieure à la version 2.440.3 LTS au 9 juillet 2024 seront supprimés des serveurs.


Cordialement,

L'équipe CI

PS: si vous avez explicitement demandé un délai pour la montée en version, ne tenez pas compte de ce message.

--------------------

Dear CI users,

Further to reminders sent by e-mail at the beginning of May and June, we would like to inform you that Jenkins servers 
with versions prior to version 2.440.3 LTS on July 9, 2024 will be removed from the servers.

Best,

The CI Team

PS: if you have explicitly requested an upgrade deadline, please disregard this message.