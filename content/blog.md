---
title: "CI blog"
sitemap:
  priority : 0.1
layout: "blog"
---


This file exists solely to respond to `/blog` URL with the related `blog` layout template.
