---
title: "Ubuntu 24.04.1"
---

# Ubuntu 24.04.1

## Packer

This template creation has been automatized thanks to [Packer](https://packer.io), and the project dedicated to its
creation is public and available at [https://gitlab.inria.fr/inria-ci/packer](https://gitlab.inria.fr/inria-ci/packer).
Do not hesitate to create issues in this project if you find any bugs in the template.

## No root user

This template has no root user for security reasons, but ci user has been granted. You can do

<code>\$ sudo my-command

Password: \*\*ci password\*\*</code>
