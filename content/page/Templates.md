---
title: "Templates"
---

# Provided featured templates (non exhaustive):

- [Windows 10](../windows10-template)
- [Mac OS X 10.9](../mac_osx_10_9)
- Fedora 33
- [Ubuntu 24.04.1](../ubuntu_24_04)

# Create your own template

If you need to do so, please read these guidelines to [create your own template](../create_community_slaves)
