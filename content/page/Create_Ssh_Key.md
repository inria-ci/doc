---
title: "Create an SSH key"
---

# Prerequisites

To use SSH to communicate with the SSH gateway **ci-ssh.inria.fr**, you need
an OpenSSH client (which comes pre-installed on GNU/Linux, macOS, and Windows
10), with an SSH version 6.5 or later (earlier versions used an MD5 signature,
which is not secure).

To view the version of SSH installed on your system, run `ssh -V`.

## Recommended SSH key types

ED25519 SSH keys:

-   more secure and performant than RSA keys.
-   OpenSSH 6.5 introduced ED25519 SSH keys in 2014 and they should be
    available on most operating systems.

RSA SSH keys:

-   RSA key generated with the Windows 10 SSH native client are not supported
    by the SSH gateway because of a
    [bug in key generation](https://github.com/PowerShell/Win32-OpenSSH/issues/1551)
    *(fixed but requires your system to be up-to-date)*
-   Since 2015, NIST recommends a minimum of 2048 bits keys for RSA
    ([Source](https://en.wikipedia.org/wiki/Key_size)).
-   The default key size depends on your version of ssh-keygen.
    Review the man page for your installed ssh-keygen command for details.

## SSH key pair generation

If you do not have an existing SSH key pair, generate a new one.

1.  Open a terminal.
2.  Type ssh-keygen -t followed by the key type and an optional comment.
    This comment is included in the `.pub` file that's created.
    It is a good practice to use your email address for the comment.

    For example, for ED25519:
    ```bash
    ssh-keygen -t ed25519 -C "<comment>"
    ```
    For 2048-bit RSA:
    ```bash
    ssh-keygen -t rsa -b 2048 -C "<comment>"
    ```
3.  Press Enter. Output similar to the following is displayed:
    ```txt
    Generating public/private ed25519 key pair.
    Enter file in which to save the key (/home/user/.ssh/id_ed25519):
    ```
4.  Accept the suggested filename and directory, unless you are generating a
    deploy key or want to save in a specific directory where you store other
    keys.
5.  Specify a [passphrase](https://www.ssh.com/ssh/passphrase/):
    ```txt
    Enter passphrase (empty for no passphrase):
    Enter same passphrase again:
    ```
6.  A confirmation is displayed, including information about where your files
    are stored.

Your private SSH key (not to be shared) is available in your home directory at
`.ssh/id_ed25519` or `.ssh/id_rsa` if you did not specify any directory.

Your public SSH key (to be
[uploaded on the portal](../web_portal_tutorial/#manage-your-account))
is available in your home directory at `.ssh/id_ed25519.pub` or
`.ssh/id_rsa.pub` if you did not specify any directory.
