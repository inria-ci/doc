---
title: "Web Portal Tutorial"
---

# Introduction

This tutorial explains how to create a project on the
**[Inria Continuous Integration Web portal](https://ci.inria.fr)**, and its
relation with CloudStack, GitLab-CI and Jenkins (*deprecated*).

## Continuous integration

Firstly, Continuous integration requires to manage all the testing process. For
example, one could want the testing process to be triggered by every commit on
a git repository, and consist in updating the sources, recompiling what has
changed, running a test suite, generating and serving HTML report, and sending
email if some tests failed. All these kind of things are managed using a
Continuous integration software.

The client-part is running on virtual machines or containers, where the tests
are effectively run. A crucial point in Continuous integration is to test on
operating systems or architectures different from the one the developer works
on. Additionally, one could want to execute tests on a fresh installation, or
to have pre-installed softwares.

The first Continuous integration solution to consider is the
[shared runner](../gitlab/#using-shared-runners-linux-only)
one that is available on the **[Inria GitLab](https://gitlab.inria.fr)**.
This software is based on the docker solution and can fulfill most of the
Continuous integration needs.
But this solution also has its limitation and it may not fit all the needs for
your Continuous integration needs (an OS that does not support docker, hard
drive size limitation, specific linux kernel tests\...).

In this case, you may consider using the
**[Inria Continuous Integration Web portal](https://ci.inria.fr)**
 to create and manage virtual machines, where all the testing process will be
 executed.  The cloud infrastructure used by Inria is **CloudStack**.

## The Web portal

In this tutorial, you will learn how to use the **Web portal** to create a
project on the Inria Continuous integration platform, and to use it to manage
your project, and reach your **CloudStack** instance. A project will consist
in one **CloudStack** domain, with multiple virtual machine instances, that are
private to each project, in the sense that a virtual machine can not be used in
two different projects. A project can regroup different developers, and a
developer can create or participate in different projects. The Web portal
provides you an interface to manage multiple projects.

# Getting started

## Create a user account

Visit the [front page](https://ci.inria.fr) of the Web portal, sign up (a valid
email address will be asked), and log in. Note that if you work at INRIA (or
any partner lab: IRISA, LORIA\...) you must enter your official e-mail address
otherwise you will not be allowed to create any project (if unsure
[make a query at this page](https://annuaire.inria.fr/)).

To log in, use the **email address** as user name.

![](/doc/img/CI-Portal-SignUp-Login.png "/doc/img/CI-Portal-SignUp-Login.png")

## The navigation bar

![](/doc/img/CI-Portal-Navigation-Bar.png "/doc/img/CI-Portal-Navigation-Bar.png")

It is worth to note that, at the top of the each page, the navigation
bar gives quick links to:

-   Sign up, Log in, Log out.
-   Dashboard.
-   Projects creation and list.
-   News.
-   Users list.
-   User account configuration (names, email, password, SSH keys\...).

Note that if your browser window is not wide enough, the navigation
buttons will not be shown. But an additional button
![](/doc/img/CI-Portal-Dashboard-Menu-Button.png "fig:/doc/img/CI-Portal-Dashboard-Menu-Button.png")
will be included in the top right. This button opens a drop-down menu
showing the navigation buttons.

![](/doc/img/CI-Portal-Dashboard-Narrow.png "/doc/img/CI-Portal-Dashboard-Narrow.png")

## Users list

The list of users of the CI portal can be opened by clicking *Users* in
the navigation bar. From here you can delete your user account.

## Manage your account

![](/doc/img/CI-Portal-MyAccount.png "/doc/img/CI-Portal-MyAccount.png")

From the navigation bar, select *My account*. Copy, paste and add now
your SSH public key, so that it can be used later to connect to the
slaves machine you will create.

![](/doc/img/CI-Portal-Ssh.png "/doc/img/CI-Portal-Ssh.png")

If you do not have an SSH key yet, you can leave it blank for the moment,
or follow the procedure to [create an SSH key pair](../create_ssh_key).

### Test your SSH key

Verify in a command line that you can connect to the SSH gateway:

```bash
ssh <User Id>@ci-ssh.inria.fr
# Enter your SSH key passphrase
# You should be logged to ci-ssh.inria.fr
```
(where User Id is the *Uid* from **My Account** page)

### SSH key propagation

The first time a user is creating his SSH key, it will only be propagated at
first [project creation](../web_portal_tutorial/#create-a-new-project)
or after [joining an existing project](../web_portal_tutorial/#join-an-existing-project).

## Create a new project

Go to the [Dashboard](https://ci.inria.fr/dashboard) page. From here,
you can create a new project, join an existing project, and manage the
project you own or participate in.

Click the green button *Host my project*.

**Note**: only INRIA users are allowed to create a project. If the
button is shown in grey (disabled), then it means that you are not
identified as an INRIA user.

![](/doc/img/CI-Portal-Dashboard-New-Project.png "/doc/img/CI-Portal-Dashboard-New-Project.png")

and fill the project creation form:

-   Shortname. It will be the identifiant (the Unix-name) of your
    project, use lower case only.
-   Fullname.
-   Description.
-   Visibility.
-   Software. If you need a Jenkins (*deprecated*) instance for your Continuous Integration,
    select "jenkins". Otherwise, select "None".

**Note:** You should open a helpdesk ticket to activate the jenkins option. See post
[Jenkins deactivation for new projects](../../posts/2023-12-18-jenkins_deactivation_for_new_projects/)

![](/doc/img/CI-Portal-Project-Creation.png "/doc/img/CI-Portal-Project-Creation.png")

Once the form submitted, you are ready to create the slaves using CloudStack. If
"jenkins" has been selected, a Jenkins master instance will be created for this
project. In this case, you will also be able to configure Jenkins.

**Note:** the creation of the project may take some time. You will
automatically receive an e-mail when the project is fully created.

## Join an existing project

*This section only applicable to **public projects**. If you want to
join a private project, then you must contact an administrator of that
project*.

To join an existing project, either click the
![](/doc/img/CI-Portal-Join-Project-Button.png "fig:/doc/img/CI-Portal-Join-Project-Button.png")
button from the dashboard, or select *list projects* from the navigation
bar. This will open the a of all public projects:

![](/doc/img/CI-Portal-Public-Projects-List.png "/doc/img/CI-Portal-Public-Projects-List.png")

From here, you can request to join a project by clicking the
![](/doc/img/CI-Portal-Join-Project-Request-Button.png "fig:/doc/img/CI-Portal-Join-Project-Request-Button.png")
button.

## Manage the project

On the Dashboard, your new project should appear, with two buttons.

![](/doc/img/CI-Portal-Dashboard.png "/doc/img/CI-Portal-Dashboard.png")

-   The
    ![](/doc/img/CI-Portal-Project-Button.png "fig:/doc/img/CI-Portal-Project-Button.png")
    button links to the **Project configuration**, a part of the Web
    portal.
-   The
    ![](/doc/img/CI-Portal-Jenkins-Button.png "fig:/doc/img/CI-Portal-Jenkins-Button.png")
    button links to the **Jenkins master** instance web interface, the
    place where all the Jenkins configuration is performed. More
    precisely, the link points to the **production** version. Production
    and qualification concept are explained below. This button shall not be here if you
    did not select "Jenkins" as a software when creating your project.

**Note:** if your project is listed as
![](/doc/img/CI-Portal-Pending.png "fig:/doc/img/CI-Portal-Pending.png"),
then it means that the creation of your project is not completed. You
will not be able to configure the project until it is listed as
![](/doc/img/CI-Portal-Active.png "fig:/doc/img/CI-Portal-Active.png").

**Depending on the load on the server, the creation of your project may
take a moment. In the meantime you can jump to the following stages:**

**Linux/MacOS Users**

1.  ensure that the **ssh** and **rdesktop** commands are installed on
    your system
2.  [create your SSH
    key](../create_ssh_key)
    (if you do not have an SSH key yet)
3.  [Configure a proxy command for
    SSH](../slaves_access_tutorial#configure-a-ssh-proxy-command "wikilink")
4.  you can start reading the [../jenkins
    tutorial](../jenkins_tutorial "wikilink")

**Windows Users**

1.  ensure that
    [**Putty**](http://www.chiark.greenend.org.uk/~sgtatham/putty/download.html)
    is installed. Especially you will need to use Putty.exe,
    Puttygen.exe, Plink.exe and (for your comfort) Pageant.exe
2.  [create your SSH
    key](../create_ssh_key)
    (if you do not have an SSH key yet)
3.  you can start reading the [../jenkins
    tutorial](../jenkins_tutorial "wikilink")

![](/doc/img/CI-Portal-Project-Abstract.png "fig:/doc/img/CI-Portal-Project-Abstract.png")

Once your projet is active, come back and continue this tutorial by
clicking the
![](/doc/img/CI-Portal-Project-Button.png "fig:/doc/img/CI-Portal-Project-Button.png")
button to open the configuration of your project. From this page, one
can get:

-   the number of slaves
-   the number of members
-   the Jenkins version. This panel shall not be here if you
    did not select "Jenkins" as a software when creating your project.
-   the log events
-   the direct access to Jenkins dashboard.

One can also edit the project or delete it if needed.

## Slaves

![](/doc/img/CI-Portal-Project-Slaves.png "fig:/doc/img/CI-Portal-Project-Slaves.png")

The **Slaves section** will display the slaves you created on CloudStack,
their status (if they are starting, running, stopping, stopped or destroyed),
and the informations required to connect the slave using SSH. You also
have the possibility to add new slave from here, stop and start existing slaves,
and delete slaves that are no longer in use,
without connecting the CloudStack platform.
For each slave, there is an expiration date: after this date, the slave will
be automatically destroyed, unless the expiration date has been extended
beforehand with the *Extend* button. Emails will be send to the project
administrators during the weeks before the expiration date to remind them to
extend their virtual machines if they still use them.

Here is an example of a slave add:

![](/doc/img/CI-Portal-NewSlave.png "fig:/doc/img/CI-Portal-NewSlave.png")

You can choose :

-   an OS template from three sources:
    -   **featured (recommended option)**: the templates supported by
        the infrastructure team
    -   **community**: the templates provided by other users
    -   **my templates**: your own templates (read the [cloudstack
        tutorial](../cloudstack_tutorial) if you want to
        create your own templates)
-   the kind of instance required for your project, in terms of CPU,
    memory and root disk size.
-   to add an additional disk.

**Please prefer to choose featured template:** They are preconfigured
to be used as slaves and are officially supported. If you wish
to use another template, then you may have to [handle extra configuration
work](../create_community_slaves).

**Please refer to [templates](../templates)** if you are facing issues.

You can act on the virtual machine with the buttons in the *Actions* column.

- When a virtual machine is running, there are two available buttons,
  *Connect* and *Stop*.

  ![](/doc/img/CI-Portal-Project-Slaves-Actions-Running.png "Actions available for running virtual machines")

  - The *Connect* button displays a popup with instructions to connect
    the virtual machine through SSH or VLC.
  - The *Stop* button stops the virtual machine.

- When a virtual machine is stopped, there are three available buttons,
  *Connect*, *Start* and *Delete*.

  ![](/doc/img/CI-Portal-Project-Slaves-Actions-Stopped.png "Actions available for stopped virtual machines")

  - The *Connect* button displays the same popup as with a running virtual
    machine, but it is worth noticing that these instructions would work
    only once the virtual machine will be restarted.
  - The *Start* button restarts the virtual machine.
  - The *Delete* button destroys the virtual machine.
    The destroyed virtual machine can still be recovered during 24 hours.

- When a virtual machine was destroyed less than 24 hours ago, there are
  two available buttons, *Connect* and *Recover*.

  ![](/doc/img/CI-Portal-Project-Slaves-Actions-Destroyed.png "Actions available for destroyed virtual machines")

  - The *Connect* button displays the same popup as with a running virtual
    machine, but it is worth noticing that these instructions would work
    only if the virtual machine is recovered.
  - The *Recover* button restores the virtual machine. Virtual machine can
    only be recovered less than 24 hours after their destruction.

### Members

![](/doc/img/CI-Portal-Project-Members.png "fig:/doc/img/CI-Portal-Project-Members.png")

This section shows all the members of the project and gives the
capability, for the administrators, to add/delete users and to modify
their rights (project administrator and slave administrator).

### Manage Jenkins

**Note:** You can [skip this part](../web_portal_tutorial/#next-steps) if you
did not select "Jenkins" as a software when creating your project.

![](/doc/img/CI-Portal-Project-Jenkins.png "fig:/doc/img/CI-Portal-Project-Jenkins.png")

The **Manage Jenkins section** offers a powerful way to secures the
changes you want to apply to your project Jenkins instance. Whether it
is a change in the Jenkins configuration (how sources are polled, how
tests are run, \...) or an upgraded of Jenkins version, or an upgrade of
a Jenkins plug-in version, all theses changes can be dangerous and make
your project Continuous integration crash. Instead of applying changes
directly on the **production Jenkins**, one can test it on a
**qualification Jenkins**. Once the qualification Jenkins is proved to
be stable, one can decide to replace the production Jenkins with it.

**Note:** when upgrading your Jenkins instance, we recommend you to opt
for a LTS release (*Long-Term Support*) rather than an ordinary release.

### Logs

![](/doc/img/CI-Portal-Project-Logs.png "fig:/doc/img/CI-Portal-Project-Logs.png")

The **Logs section** lists the last events or actions that occured
(slave creation, jenkins version update, \...).

### Jenkins Dashboard

The **Jenkins Dashboard section** enables a
direct access to the Jenkins interface where your project is handled.

# Next Steps

You have now completed the first step of the tutorial!

The Web Portal addresses only the most common use case in managing your
slaves (Create from a template, Start, Stop, Delete, Edit the
description). If this is sufficent for you, then you should skip
directly to Step 2, otherwise you may want to go through Step 1 bis
first.

## Step 1 bis (optional) Tuning your slaves

[→ Cloudstack Tutorial](../cloudstack_tutorial "wikilink")

This part covers advanced scenarios for managing your slaves:

-   creating a slave from scratch (using an ISO)
-   creating a new template
-   adding storage space to an existing slave

## Step 2: Connecting to your slaves

[→ Slaves Access Tutorial](../slaves_access_tutorial "wikilink")

This part will teach you how to open a remote session on your virtual
machines. This will allow you to handle the administration tasks (change
the passwords, install new packages, \...). Linux slaves are accessed
via an SSH session and Windows slaves via a Remote Desktop session.
