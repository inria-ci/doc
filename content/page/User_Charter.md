---
title: "User Charter"
---
**The objectives of this charter are:**

-   to recall the legislative framework and the general deontology that
    applies to information made available on the Internet;
-   to define the conditions for hosting projects on the Inria
    Continuous Integration server;
-   to define the responsibilities accepted when using the Inria
    Continuous Integration service, which is available on the server
    ci.inria.fr, and the associated Cloud platform, available on
    ci-cloud.inria.fr.

The general legislative framework
=================================

The opening of a project and its activity are regulated by the laws
relative to the dissemination and diffusion of intellectual works. This
implies, particularly, the respect of the following obligations:

-   To obtain from the companies and establishments the necessary rights
    from the authors, the right to reproduce and to disseminate written
    works, photographs, or music that are protected by author rights or
    any associated/annex rights.
-   Respect of personal rights (image/photo, privacy protection);
    respect of the exigencies of the law "Computers and Liberty" and
    those laws applicable to audiovisual diffusion.
-   Respect of European law: in accordance with the European directives
    (Directive 95/46/CE of the European parliament and the consul of 24
    October 1995), each person must give his written accord if personal
    data is accessible on the Internet.

Conditions for hosting projects on the Inria Continuous Integration service
===========================================================================

The Inria Continuous Integration service is open to projects involving
at least one person working at the institute. At least, this person is
identified as a responsible for the project (project administrator).
However, several administrators can be responsible for a given project.

User responsibility
===================

The access to the Cloud platform ci-cloud.inria.fr is reserved for
Continuous Integration purposes and is not supposed to be used as a
global computing resource or as a network service provider.

A person with access rights promises to not divulge the parameters that
permit one to access his account on the ci.inria.fr or ci-cloud.inria.fr
sites. This right cannot be delegated to another person.

The Inria Continuous Integration service is a shared service. As a
consequence, users are supposed to use the service with reasonable and
due care to not interfere with or disturb other users.

Furthermore, users of the Inria Continuous Integration service are
responsible for:

-   acquiring the software licenses possibly required on the nodes
    belonging to the Cloud platform provided through ci-cloud.inria.fr
    (the operating system is also concerned). For instance, using a
    proprietary compiler like the Intel C Compiler implies that you own
    the related license;
-   securing their Cloud nodes using any available and appropriate
    protection (e.g. firewall, antivirus, up-to-date operating system
    and software, etc);
-   the Internet accesses performed from their Cloud nodes;
-   the protection of the data they put on the service.

**Any failure to comply with this charter could result in penalties
(account and data deletion) and possibly legal prosecutions.**

Reminder of the principle laws
==============================

Inria is governed by French law. You must therefore abide by the law or
face the risk of criminal proceedings. For more information, see
(non-exhaustive list):

-   «Loi Informatique et liberté n°78-17» of 6 January 1978 (Information
    technology, data files and civil liberties)
-   Legislation concerning intellectual property, including «Loi n°
    2006-961 relative au droit d'auteur et aux droits voisins dans la
    société de l'information» of 1st August 2006 (Copyright and Related
    Rights in the Information Society)
-   «Loi n° 2004-575 pour la confiance dans l'économie numérique» of
    21st June 2004 (Trust in digital economy).
-   Legislation on broadcasting and telecommunications concerning the
    guiding principles applicable to public and private communications.
