FROM alpine:latest
RUN apk add rustup go cargo git
RUN cargo install toml-cli
RUN GOBIN=/bin/ go install github.com/gohugoio/hugo@v0.106.0
